// 
// 
// 

#include "WiFiManagerConfig.h"


WiFiManagerConfig::WiFiManagerConfig()
{
	EEPROM.begin(EEPROM_SIZE);
	setup();
}

void WiFiManagerConfig::setSsid(String ssid)
{
	_ssid = ssid;
}

void WiFiManagerConfig::setPassword(String password)
{
	_password = password;

}

String WiFiManagerConfig::getSsid()
{
	return _ssid;
}

String WiFiManagerConfig::getPassword()
{
	return _password;
}

char* WiFiManagerConfig::read()
{
	char wiFiConfig[EEPROM_SIZE];
	char value;
	for (int i = 0; i < EEPROM_SIZE; i++) {
		value = EEPROM.read(i);
		wiFiConfig[i] = value;
		if (value == '\0') break;
	}
	return wiFiConfig;
}


bool WiFiManagerConfig::save()
{

	if (_ssid == "" || _ssid == NULL) {
		return false;
	}
	else
	{
		Json json;
		json.add("ssid", _ssid);
		json.add("password", _password);
		String configJsonStr = json.toString();
		Serial.println(configJsonStr);
	
		clear();

		for (int i = 0; i < configJsonStr.length(); i++) {
			EEPROM.write(i, configJsonStr.charAt(i));
		}
		EEPROM.commit();
		return true;

	}


}

bool WiFiManagerConfig::isExist()
{
	String wiFiConfig = read();

	Json json;
	
	return  json.parse(wiFiConfig) && json.get("ssid") != "";
}

bool WiFiManagerConfig::setup()
{

	String wiFiConfig = read();
	Serial.println(wiFiConfig);
	Json json;
	

	if (json.parse(wiFiConfig) && json.get("ssid") != "") {
		_ssid = json.get("ssid");
		_password = json.get("password");
		return true;
	}
	else
	{
		return false;
	}
	

}

void WiFiManagerConfig::clear()
{
	//_ssid = "";
	//_password = "";

	for (int i = 0; i < EEPROM_SIZE; i++) {
		EEPROM.write(i, 0);
	}

	EEPROM.commit();
}

bool WiFiManagerConfig::isValid()
{
	if (_ssid != "" ) {
		return true;
	}
	else
	{
		return false;
	}
	
}