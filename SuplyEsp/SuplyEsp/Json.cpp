// 
// 
// 

#include "Json.h"


Json::Json()
{
	_map = new HashMap();
}

Json::~Json()
{
	delete _map;
}

String Json::toString() {
	const unsigned int sizeBuffer = getMapLenght();
	DynamicJsonBuffer jsonBuffer;
	jsonBuffer.alloc(sizeBuffer);
	JsonObject& root = jsonBuffer.createObject();
	JsonObject* rootPointner = &root;

	for (int i = 0; i < _map->size(); i++) {
		jsonObjectFromMap(rootPointner, _map->get(i));
	}

	return toString(root);
}

String Json::toString(JsonObject & json)
{
	size_t jsonLenght = json.measureLength();
	char *buffer = new char[++jsonLenght];

	json.printTo(buffer, jsonLenght);

	String str = buffer;
	delete[] buffer;

	return str;
}

JsonObject& Json::parseObject(String jsonStr)
{
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& json = jsonBuffer.parseObject(jsonStr);
	return json;
}

bool Json::parse(String jsonStr)
{

	_root = &parseObject(jsonStr);
	

	return _root->success();

}

JsonObject & Json::createObject()
{
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();

	return root;
}
/*

void Json::add(String key, const char* value)
{
	Serial.println("---------");
	Serial.println(value);
	_map->push(key, value);
}
*/


void Json::add(String key, String value)
{
	_map->push(key, value);
}

void Json::add(String key, int value)
{
	_map->push(key, value);
}

void Json::add(String key, float value)
{
	_map->push(key, value);
}

void Json::add(String key, Json &value)
{
	Json *val = &value;
	_map->push(key, val->_map);
}

const char * Json::get(String key)
{
		const char* value = (*_root)[key];
	
		return value;
}

void Json::setArrayObjectSize(unsigned int arrayObjectSize)
{
	_arrayObjectSize = arrayObjectSize;
}

void Json::jsonObjectFromMap(JsonObject* root, HashMapType type)
{
	switch (type.getType())
	{
	case HashMapType::CONSTCHAR:
		(*root)[type.getKey()] = type.getConstCharValue();
		break;
	case HashMapType::STRING:
		(*root)[type.getKey()] = type.getStringValue();
		break;
	case HashMapType::INTAGER:
		(*root)[type.getKey()] = type.getIntValue();
		break;
	case HashMapType::FLOAT:
		(*root)[type.getKey()] = type.getFloatValue();
		break;
	case HashMapType::HASHMAP:
		JsonArray& list = root->createNestedArray(type.getKey());
		if (_arrayObjectSize > 0) {
			int i = 0;
			while (i < (type.getHasMapValue()->size())) {
				JsonObject& nested = list.createNestedObject();
				JsonObject* nestedPointner = &nested;
				for (int j = 0; j < _arrayObjectSize; j++) {
					jsonObjectFromMap(nestedPointner, type.getHasMapValue()->get(i));
					i++;
				}

			}
		}



		break;
	}

}

int Json::getMapLenght()
{
	int lenght = _map->getLenght();
	for (int i = 0; i < _map->size(); i++)
	{
		if (_map->get(i).getType() == HashMapType::HASHMAP) {
			lenght += _map->get(i).getHasMapValue()->getLenght();
			lenght += _map->get(i).getHasMapValue()->size();
		}
	}
	return lenght;
}