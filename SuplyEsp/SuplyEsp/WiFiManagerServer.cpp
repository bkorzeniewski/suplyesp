// 
// 
// 

#include "WiFiManagerServer.h"



WiFiManagerServer::WiFiManagerServer()
{
}

WiFiManagerServer::~WiFiManagerServer()
{
	delete _dnsServer, _server;
}

void WiFiManagerServer::setup()
{
	_dnsServer = new DNSServer();

	_server = new ESP8266WebServer(_serverPort);

	_dnsServer->setTTL(_dnsTTL);

	_dnsServer->setErrorReplyCode(DNSReplyCode::ServerFailure);

	_dnsServer->start(_dnsPort, "*", _ipHostname);

	_server->on("/", std::bind(&WiFiManagerServer::checkDns, this));

	_server->onNotFound(std::bind(&WiFiManagerServer::dnsForwarding, this));
	
	_server->begin();
}

void WiFiManagerServer::loop()
{
	_dnsServer->processNextRequest();

	_server->handleClient();
}

void WiFiManagerServer::dnsForwarding()
{
		_server->sendHeader("Location", "http://" + _dnsHostname);
		_server->send(301);		
	
}

void WiFiManagerServer::checkDns()
{
	if (_server->hostHeader() != _dnsHostname) {
		dnsForwarding();
	}
	else {
		configPortalPage();
	}
}

void WiFiManagerServer::configPortalPage()
{
		_server->send(200, "text/html", "<html><head><script>var connection = new WebSocket('ws://'+location.hostname+':81/', ['arduino']);connection.onopen = function () {  connection.send('Connect ' + new Date()); }; connection.onerror = function (error) {    console.log('WebSocket Error ', error);};connection.onmessage = function (e) {  console.log('Server: ', e.data);};function sendRGB() {  var r = parseInt(document.getElementById('r').value).toString(16);  var g = parseInt(document.getElementById('g').value).toString(16);  var b = parseInt(document.getElementById('b').value).toString(16);  if(r.length < 2) { r = '0' + r; }   if(g.length < 2) { g = '0' + g; }   if(b.length < 2) { b = '0' + b; }   var rgb = '#'+r+g+b;    console.log('RGB: ' + rgb); connection.send(rgb); }</script></head><body>TEST SERVER LED Control:<br/><br/>R: <input id=\"r\" type=\"range\" min=\"0\" max=\"255\" step=\"1\" onchange=\"sendRGB();\" /><br/>G: <input id=\"g\" type=\"range\" min=\"0\" max=\"255\" step=\"1\" onchange=\"sendRGB();\" /><br/>B: <input id=\"b\" type=\"range\" min=\"0\" max=\"255\" step=\"1\" onchange=\"sendRGB();\" /><br/></body></html>");	
}
