// WiFiManager.h

#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <Hash.h>

#include "WiFiManagerConfig.h"
#include "WiFiManagerServer.h"


#ifndef _WIFIMANAGER_h
#define _WIFIMANAGER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


#endif

#define JSON_BUFFER_SIZE 100

#define TIMEOUT_CHECKING 10



class WiFiManagerSettings
{
public:
	String deviceId = String(ESP.getChipId());
	String deviceName = "Suply-" + deviceId;
	const char* apSsid = deviceName.c_str();
	const char* apPassword = "12345678";
	IPAddress apIp = IPAddress(192, 168, 1, 1);
	IPAddress apSubnet = IPAddress(255, 255, 255, 0);
	IPAddress apGateway = IPAddress(192, 168, 1, 1);

};


class WiFiManagerWebSocket;

class WiFiManager
{
public:
	WiFiManager();
	~WiFiManager();
	void setup();
	void loop();
	bool setupConfigPortal();
	void startWiFiClient();
	void setApList();
	String getApList();
	void setWiFiConnectionStatus(int status = 0);
	String getWiFiConnectionStatus();
	void connect(String ssid, String password);
	void disconnect();
	WiFiManagerConfig* getWiFiConfig();

	void reboot();

private:
	WiFiManagerSettings _settings;
	WiFiManagerServer* server;
	WiFiManagerWebSocket* webSocket;
	WiFiManagerConfig* wiFiConfig;
	bool _startConfigPortal = false;
	String _apList;
	unsigned int _wiFiConnectionStatus = 0;
	WiFiEventHandler _gotIpEventHandler, _disconnectedEventHandler;


};


