//HashMap.h

#ifndef _HASHMAP_h
#define _HASHMAP_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


#endif

class HashMap;
class HashMapType
{
public:

	enum Type
	{
		STRING,
		CONSTCHAR,
		INTAGER,
		FLOAT,
		BOOL,
		HASHMAP
	};

	HashMapType() {}
	~HashMapType() {}
	HashMapType(String key, int value) {
		_key = key;
		_intValue = value;
		_type = INTAGER;
	}
	HashMapType(String key, float value) {
		_key = key;
		_floatValue = value;
		_type = FLOAT;
	}

	HashMapType(String key, bool value) {
		_key = key;
		_boolValue = value;
		_type = FLOAT;
	}

	HashMapType(String key, const char* value) {
		_key = key;
		_constCharValue = value;
		_type = CONSTCHAR;
	}

	HashMapType(String key, String value) {
		_key = key;
		_stringValue = value;
		_type = STRING;
	}

	HashMapType(String key, HashMap* value) {
		_key = key;
		_hasMapValue = value;
		_type = HASHMAP;
	}

	int getIntValue() {
		return _intValue;
	}

	float getFloatValue() {
		return _floatValue;
	}

	bool getBoolValue() {
		return _boolValue;
	}

	const char * getConstCharValue() {
		return _constCharValue;
	}

	String getStringValue() {
		return _stringValue;
	}

	HashMap* getHasMapValue() {
		return _hasMapValue;
	}
	String getKey() {
		return _key;
	}
	Type getType() {
		return _type;
	}

private:

	String _key;
	int _intValue;
	float _floatValue;
	bool _boolValue;
	const char* _constCharValue;
	String _stringValue;
	HashMap* _hasMapValue;
	Type _type;

};

class HashMap {
public:
	HashMap() {
		_map = new HashMapType[_size];
	}

	~HashMap() {
		delete[] _map;
	}

	void push(String key, int value) {

		pushType(HashMapType(key, value));

	}

	void push(String key, const char* value) {

		pushType(HashMapType(key, value));

	}

	void push(String key, String value) {

		pushType(HashMapType(key, value));

	}

	void push(String key, float value) {

		pushType(HashMapType(key, value));

	}

	void push(String key, HashMap* value) {

		pushType(HashMapType(key, value));

	}

	int size() {
		return _size - 1;
	}
	HashMapType operator[](int x) {
		return _map[x];
	}

	HashMapType get(int x) {
		return _map[x];
	}

	int getLenght() {
		int lenght = 0;
		for (int i = 0; i < size(); i++) {
			lenght += _map->getKey().length() + 3;
			if (_map->getIntValue() != NULL) {
				lenght += String(_map->getIntValue()).length();
			}
			else if (_map->getFloatValue() != NULL)
			{
				lenght += String(_map->getFloatValue()).length();
			}
			else if (_map->getStringValue() != NULL)
			{
				lenght += _map->getStringValue().length() + 2;
			}
			else if (_map->getBoolValue() != NULL)
			{
				lenght += String(_map->getBoolValue()).length() + 2;
			}
			lenght += 1;
		}

		return lenght + 2;
	}
private:
	HashMapType* _map;
	int _size = 1;

	void pushType(HashMapType hashMapType) {
		_map[_size - 1] = hashMapType;

		HashMapType* newMap = new  HashMapType[++_size];
		for (int i = 0; i < _size - 1; i++)
		{
			newMap[i] = _map[i];
		}
		delete[] _map;
		_map = newMap;


	}
};