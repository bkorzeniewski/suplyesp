// Json.h
#include <ArduinoJson.h>
#include "HashMap.h"


#ifndef _JSON_h
#define _JSON_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


#endif
#define JSON_BUFFER_SIZE



class Json
{
public:
	Json();
	~Json();
	String toString(JsonObject& json);
	String toString();
	JsonObject& parseObject(String jsonStr);
	bool parse(String jsonStr);
	JsonObject& createObject();

	void add(String key, String value);
	void add(String key, int value);
	void add(String key, float value);
	void add(String key, Json &value);
	const char* get(String key);
	void setArrayObjectSize(unsigned int arrayObjectSize);

private:
	HashMap* _map;
	JsonObject* _root;
	unsigned int _arrayObjectSize = 0;
	void jsonObjectFromMap(JsonObject* root, HashMapType type);
	int getMapLenght();
};


