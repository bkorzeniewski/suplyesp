// WiFiManagerServer.h

#include <DNSServer.h>
#include <ESP8266WebServer.h>

#ifndef _WIFIMANAGERSERVER_h
#define _WIFIMANAGERSERVER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#endif

class WiFiManagerServer
{
public:
	WiFiManagerServer();
	~WiFiManagerServer();
	void setup();
	void loop();

private:
	DNSServer* _dnsServer;
	ESP8266WebServer* _server;
	unsigned int _serverPort = 80;
	unsigned int _dnsPort = 53;
	String _dnsHostname = "suply.io";
	IPAddress _ipHostname = IPAddress(192, 168, 1, 1);
	unsigned int _dnsTTL = 300;
	void dnsForwarding();
	void checkDns();
	void configPortalPage();
};


