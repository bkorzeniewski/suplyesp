// 
// 
// 

#include "WiFiManager.h"
#include "WiFiManagerWebSocket.h"

WiFiManager::WiFiManager()
{
	server = new WiFiManagerServer();
	webSocket = new WiFiManagerWebSocket(this);
	wiFiConfig = new WiFiManagerConfig();

}

WiFiManager::~WiFiManager()
{
	delete server, webSocket, wiFiConfig;
}

void WiFiManager::setup()
{
	_gotIpEventHandler = WiFi.onStationModeGotIP([=](const WiFiEventStationModeGotIP& event)
	{
		_wiFiConnectionStatus = WIFI_EVENT_STAMODE_GOT_IP;
		webSocket->setStatus(getWiFiConnectionStatus());
		Serial.println("----config---");
		Serial.println(wiFiConfig->getSsid());
		Serial.println(wiFiConfig->getPassword());
		Serial.println("----END config---");
		wiFiConfig->save();
		reboot();

	});

	_disconnectedEventHandler = WiFi.onStationModeDisconnected([=](const WiFiEventStationModeDisconnected& event)
	{
		_wiFiConnectionStatus = event.reason;
		
		webSocket->setStatus(getWiFiConnectionStatus());
		if (event.reason != WIFI_EVENT_STAMODE_DISCONNECTED && 
			event.reason != WIFI_DISCONNECT_REASON_ASSOC_LEAVE) {
			WiFi.disconnect();
		}
		
	
	});
	
	wiFiConfig->setup();
	Serial.println("----config---");
	Serial.println(wiFiConfig->getSsid());
	Serial.println(wiFiConfig->getPassword());
	Serial.println("----END config---");

	setupConfigPortal();


}

void WiFiManager::loop()
{
	if (_startConfigPortal) {

		server->loop();

		webSocket->loop();

	}
}

bool WiFiManager::setupConfigPortal()
{
	WiFi.mode(WIFI_AP_STA);

	WiFi.softAPConfig(_settings.apIp, _settings.apGateway, _settings.apSubnet);

	WiFi.softAP(_settings.apSsid, _settings.apPassword);

	WiFi.setAutoConnect(false);

	WiFi.setAutoReconnect(false);

	server->setup();

	webSocket->setup();

	setApList();

	_startConfigPortal = true;

	return true;
}

void WiFiManager::startWiFiClient()
{
	WiFi.hostname(_settings.deviceName);

	WiFi.mode(WIFI_STA);

	WiFi.begin("", "");
}

void WiFiManager::setApList()
{
	Json json;
	Json ap;

	int n = WiFi.scanNetworks();
	if (n)
	{
		for (int i = 0; i < n; ++i)
		{
			ap.add("ssid", WiFi.SSID(i));
			ap.add("rssi", WiFi.RSSI(i));
			ap.add("authmode", (WiFi.encryptionType(i) != ENC_TYPE_NONE));
		}

		json.setArrayObjectSize(3);
		json.add("access_point_list", ap);
	}
	else
	{
		json.add("access_point_list", "not_found");

	}


	_apList = json.toString();

}

String WiFiManager::getApList()
{
	return _apList;
}

void WiFiManager::setWiFiConnectionStatus(int status)
{
	_wiFiConnectionStatus = status;
}

String WiFiManager::getWiFiConnectionStatus() {
	if (!_wiFiConnectionStatus) return "WAIT";

	String status;

	switch (_wiFiConnectionStatus) {
	case WIFI_EVENT_STAMODE_GOT_IP:
		status = "CONNECTED";
		break;
	case WIFI_EVENT_STAMODE_DISCONNECTED:
		status = "LOST_CONNECTED";
		break;
	case WIFI_DISCONNECT_REASON_ASSOC_LEAVE:
		status = "LOST_CONNECTED";
		break;
	case WIFI_DISCONNECT_REASON_NO_AP_FOUND:
		status = "NO_AP_FOUND";
		break;
	case WIFI_DISCONNECT_REASON_AUTH_FAIL:
		status = "AUTH_FAIL";
		break;
	default:
		status = "TIMEOUT";
		break;
	}
	return status;
}

void WiFiManager::connect(String ssid, String password)
{
	wiFiConfig->setSsid(ssid);
	wiFiConfig->setPassword(password);
	WiFi.begin(ssid.c_str(), password.c_str());
}

void WiFiManager::disconnect()
{
	if (WiFi.isConnected()) {
		WiFi.disconnect();
	}
	
}

WiFiManagerConfig * WiFiManager::getWiFiConfig()
{
	return wiFiConfig;
}

void WiFiManager::reboot()
{

	webSocket->send("REBOOT");
	//delay(5000);
	//ESP.reset();
	//delay(2000);
	
}

