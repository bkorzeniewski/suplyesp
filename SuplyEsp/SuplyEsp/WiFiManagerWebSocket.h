// WiFiManagerWebSocket.h

#include <WebSocketsServer.h>


#ifndef _WIFIMANAGERWEBSOCKET_h
#define _WIFIMANAGERWEBSOCKET_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


#endif

class Json;
class WiFiManager;

class WiFiManagerWebSocket
{
public:
	WiFiManagerWebSocket(WiFiManager* wiFiManager);
	void setup();
	void loop();

	void send(String message);
	void setStatus(String status);


private:
	enum Controllers
	{
		APS_LIST = 1,
		SAVE_AP = 2,
	};
	unsigned int _webSocketPort = 81;
	WiFiManager* wiFiManager;
	WebSocketsServer* webSocket;
	String _status;
	void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght);
	void sendWiFiConnectionStatus(uint8_t num = NULL);
	void controller(uint8_t num,String request);
	//void saveAp(const char* const &ssid, const char* const & password);
};
