// 
// 
// 

#include "WiFiManagerWebSocket.h"
#include "WiFiManager.h"

WiFiManagerWebSocket::WiFiManagerWebSocket(WiFiManager* wiFiManager)
{
	this->wiFiManager = wiFiManager;
}


void WiFiManagerWebSocket::setup()
{
	webSocket = new WebSocketsServer(_webSocketPort);

	webSocket->begin();

	webSocket->onEvent(
		std::bind(&WiFiManagerWebSocket::webSocketEvent, this,
			std::placeholders::_1,
			std::placeholders::_2,
			std::placeholders::_3,
			std::placeholders::_4));

}

void WiFiManagerWebSocket::loop()
{
	webSocket->loop();
}


void WiFiManagerWebSocket::send(String message)
{
	webSocket->broadcastTXT(message);
}

void WiFiManagerWebSocket::setStatus(String status)
{
	_status = status;

	if (WiFi.isConnected()) {
		sendWiFiConnectionStatus();
	}

}

void WiFiManagerWebSocket::sendWiFiConnectionStatus(uint8_t num)
{
	if (_status != "") {
		num != NULL ? webSocket->sendTXT(num, _status) : webSocket->broadcastTXT(_status);
		wiFiManager->setWiFiConnectionStatus(0);
		_status = wiFiManager->getWiFiConnectionStatus();
	}


}


void WiFiManagerWebSocket::webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght)
{
	switch (type) {
	case WStype_DISCONNECTED:
		break;
	case WStype_CONNECTED:
		sendWiFiConnectionStatus(num);
		break;
	case WStype_TEXT: {
		String request;
		for (int i = 0; i < lenght; i++) {
			request += (char)payload[i];
		}


		controller(num, request);

	}

					  break;
	}
}



void WiFiManagerWebSocket::controller(uint8_t num, String request)
{
	Json jsonParm;
	if (jsonParm.parse(request)) {

		int controller = atoi(jsonParm.get("controller"));

		switch (controller)
		{
		case APS_LIST: {
			String response = wiFiManager->getApList();
			webSocket->sendTXT(num, response);
			wiFiManager->setApList();
			break;
		}
		case SAVE_AP: {
			String ssid = jsonParm.get("ssid");
			String password = jsonParm.get("password");
			
			wiFiManager->disconnect();
			wiFiManager->connect(ssid, password);

			break;
		}

		default: {
			Json json;
			json.add("error", "not_found");
			String response = json.toString();
			webSocket->sendTXT(num, response);
			break;
		}
		}

	}
	
}