// WiFiManagerConfig.h

#include <EEPROM.h>
#include "Json.h"

#ifndef _WIFIMANAGERCONFIG_h
#define _WIFIMANAGERCONFIG_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#endif
#define EEPROM_SIZE 512

class WiFiManagerConfig
{
public:

	WiFiManagerConfig();
	void setSsid(String ssid);
	void setPassword(String password);
	String getSsid();
	String getPassword();
	char* read();
	//bool save(String ssid, String password);
	bool save();
	bool isExist();
	bool setup();
	void clear();
	bool isValid();
	
private:
	String _ssid = "";
	String _password = "";
};